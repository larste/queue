package org.bitbucket.larste.util.queue;

public class ArrayQueue<T> implements Queue<T> 
{
	private Object[] array = new Object[4];
	private int size = 0;
	private int current = 0;

	@Override
	public T next() 
	{
		if ( current >= size() )
		{
			current = 0;
		}
		
		T result = (T) array[current];
		current++;	
		
		return result;
	}
	
	@Override
	public T remove() throws EmptyQueueException 
	{
		if ( isEmpty() )
		{
			throw new EmptyQueueException();
		}
		
		int newSize = size() == 1 ? 1 : size() - 1;
		
		Object[] tmpArray = new Object[newSize];
		
		for ( int i = 1; i < size(); i++ )
		{
			tmpArray[i-1] = array[i];
		}
		
		T removed = (T) array[0];
		
		array = tmpArray;
		size--;
		
		return removed;
	}

	@Override
	public void add(T item) 
	{
		if ( size() == array.length )
		{
			array = increaseArray(array);
		}
		
		array[size()] = item;
		size++;
	}

	@Override
	public int size() 
	{
		return size;
	}

	@Override
	public boolean isEmpty() 
	{
		return size() == 0;
	}
	
	private Object[] increaseArray(Object[] arr)
	{
		Object[] tmp = new Object[array.length * 2];
		
		for ( int i = 0; i < size(); i++ )
		{
			tmp[i] = array[i];
		}
		
		return tmp;
	}
}