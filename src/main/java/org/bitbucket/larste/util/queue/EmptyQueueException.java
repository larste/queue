package org.bitbucket.larste.util.queue;

public class EmptyQueueException extends Exception 
{
	public EmptyQueueException(String message)
	{
		super(message);
	}
	
	public EmptyQueueException(String message, Throwable throwable)
	{
		super(message, throwable);
	}

	public EmptyQueueException() 
	{
		super();
	}
}