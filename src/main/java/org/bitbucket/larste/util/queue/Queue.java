package org.bitbucket.larste.util.queue;

public interface Queue<T> 
{
	/**
	 * Returns the next element of the queue
	 * @return T
	 */
	public T next();
	
	/**
	 * Removes and returns the next element in the queue
	 * @return T
	 * @throws Exception 
	 */
	public T remove() throws EmptyQueueException;
	
	/**
	 * Adds an element to the back of the queue
	 * @param item
	 */
	public void add(T item);
	
	/**
	 * Returns number of elements in queue
	 * @return
	 */
	public int size();
	
	/**
	 * Returns true if queue is empty, otherwise true
	 * @return boolean
	 */
	public boolean isEmpty();
}