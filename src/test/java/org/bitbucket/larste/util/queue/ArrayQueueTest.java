package org.bitbucket.larste.util.queue;

import static org.junit.Assert.assertEquals;

import org.bitbucket.larste.util.queue.ArrayQueue;
import org.bitbucket.larste.util.queue.EmptyQueueException;
import org.bitbucket.larste.util.queue.Queue;
import org.junit.Before;
import org.junit.Test;

public class ArrayQueueTest 
{
	private Queue<String> q;
	
	@Before
	public void setup()
	{
		q = new ArrayQueue<String>();
	}
	
	@Test
	public void addToQueueAddsObject()
	{
		String str = "String";
		q.add(str);
		assertEquals(str, q.next());
	}
	
	@Test
	public void addToQueueIncreasesSize()
	{
		String str = "String";
		q.add(str);
		assertEquals(1, q.size());
	}
	
	@Test
	public void nextReturnsObject()
	{
		String str = "String";
		q.add(str);
		assertEquals(str, q.next());
	}
	
	@Test
	public void removeFromQueueReturnsObject() throws EmptyQueueException
	{
		String str = "String";
		q.add(str);
		String result = q.remove();
		assertEquals(str, result);
	}
	
	@Test
	public void removeFromQueueDecreasesSize() throws EmptyQueueException
	{
		String str = "String";
		q.add(str);
		int before = q.size();
		q.remove();
		int after = q.size();
		assertEquals(before, after + 1);
	}
	
	@Test(expected=EmptyQueueException.class)
	public void removeOnEmptyQueueThrowsEmptyQueueException() throws EmptyQueueException 
	{
		q.remove();
	}
}