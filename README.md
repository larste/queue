## Queue
A couple of implementations of a generic Queue in Java

### Examples
	
	:::java
	// Queue implementation using an array
	Queue<String> queue = new ArrayQueue<String>();
	
	queue.add("Hello World!");
	System.out.println(queue.next()); // Prints out Hello World!
	
	String removed = q.remove();
	System.out.println(removed); // Prints out Hello World!
	

### License
The Queue package is open-sourced software licensed under the [MIT licence](http://larste.mit-license.org)